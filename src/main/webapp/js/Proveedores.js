var appProveedores = angular.module('appProveedores',[]);

appProveedores.controller('CtrlProveedor',['$scope','$http','$log',
    function($scope,$http,$log){
        
        $log.debug('Empezando a definir el Controlador...');
        
        $scope.listaProveedores = [];
        $scope.cargarProveedores = function(){
            $http.get('SAI2018/Proveedores')
                    .then(function(respuesta){
                        $scope.listaProveedores = respuesta.data;
                        $log.debug('Datos de Proveedores : ' + respuesta.data);
                    });
        };
        
        $scope.idProveedor= 1;
        $scope.UnProveedor= {};
        $scope.cargaUNProveedor = function(){
            $log.debug('cargarUNProveedor.');
            $http.get('SAI2018/Proveedores',$scope.idProveedor)
                    .then(function(respuesta){
                        $log.debug('get(1) exitoso');
                        $scope.UnProveedor= respuesta.data;
                    },function(respuesta){
                        $log.debug('ERROR en get(1).');
                    });
        };
               
        
        $scope.nuevoProveedor= {};
        $scope.guardarProveedor = function(){
            $log.debug('guardarProveedores.');
            $http.post('SAI2018/Proveedores',$scope.nuevoProveedor)
                    .then(function(respuesta){
                        $log.debug('POST exitoso');
                        $scope.nuevoProveedor={};
                        $scope.cargarProveedores();
                    },function(respuesta){
                        $log.debug('POST ERROR.');
                    });
        };
        
        $scope.idProveedor= 0;
        $scope.UNnuevoProveedor={};
        $scope.putProveedor = function(){
            $log.debug('guardarUNproveedor. ');
            $http.put('SAI2018/Proveedores',$scope.idProveedor,$scope.UNnuevoProveedor)
                    .then(function(respuesta){
                        $log.debug('PUT exitoso');
                        $scope.UNnuevoProveedor={};
                        $scope.cargarProveedores(); 
                    },function(respuesta){
                        $log.debug('ERROR en PUT.');
                    });
        };
        
        $scope.idProveedor= 0;
        $scope.eliminarProveedor = function(){
            $log.debug('eliminarProveedor. ');
            $http.delete('SAI2018/Proveedores',$scope.idProveedor)
                    .then(function(respuesta){
                        $log.debug('DELETE exitoso');
                        $scope.nuevoProveedor={};
                        $scope.cargarProveedores();
                    },function(respuesta){
                        $log.debug('ERROR en DELETE.');
                    });
        };
        
        
 }]);