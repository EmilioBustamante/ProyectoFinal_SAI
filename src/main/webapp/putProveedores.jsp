<%-- 
    Document   : putAutos
    Created on : 09-dic-2017, 17:21:15
    Author     : EmilioIsmael
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html ng-app="appProveedores">
    <head>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.6/angular.js"></script>
        <script src="js/Proveedores.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Proveedores del Estado de Veracruz de Ignacio de la Llave</title>
    </head>
    <body>
        <h1>Proveedor Estatal</h1>
        <div ng-controller="CtrlProveedor">
            <table>
                <thead>
                    <tr>
                       <th>ID_PROVEEDOR</th>
                       <th>NOMBRE</th>
                       <th>TIPO_PROVEEDOR</th>
                       <th>DOMICILIO_FISCAL</th>
                       <th>TELEFONO_FIJO</th>
                       <th>EMAIL</th>
                       <th>BAJALOGICA</th>
                       <th>FECHA_REGISTRO</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="a in listaProveedores">
                        <td>{{a.id}}</td>
                        <td>{{a.nombre}}</td>
                        <td>{{a.tipoproveedor}}</td>
                        <td>{{a.domiciliofiscal}}</td>
                        <td>{{a.telefonofijo}}</td>
                        <td>{{a.email}}</td>
                        <td>{{a.bajalogica}}</td>
                        <td>{{a.fecharegistro | date : 'dd/MM/yyyy' }}</td>
                    </tr>
                </tbody>
            </table>
            <br/>
        <label>ID a actualizar:</label>
        <input type ="number" ng-model="idProveedor"><br/>
        <label>Nombre:</label>
        <input type="text" ng-model="nuevoProveedor.nombre"><br/>
        <label>Tipo Proveedor:</label>
        <input type="number" ng-model="nuevoProveedor.tipoproveedor"><br/>
        <label>Domicilio Fiscal:</label>
        <input type="text" ng-model="nuevoProveedor.domiciliofiscal"><br/>
        <label>Teléfono Fijo:</label>
        <input type="text" ng-model="nuevoProveedor.telefonofijo"><br/>
        <label>Email:</label>
        <input type="text" ng-model="nuevoProveedor.email"><br/>
        <label>BajaLogica:</label>
        <input type="number" ng-model="nuevoProveedor.bajalogica"><br/>
        <label>Fecha_Renta:</label>
        <input type="date" ng-model="nuevoProveedor.fechaRenta"><br/>
        <br/>
        <input type="button" value="Actualizar" ng-click="putProveedor()"> 
        
        </div>
        
    </body>
</html>
