<%-- 
    Document   : getUNauto
    Created on : 09-dic-2017, 17:32:58
    Author     : EmilioIsmael
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html ng-app="appProveedores">
    <head>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.6/angular.js"></script>
        <script src="js/Proveedores.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Proveedores de Gobierno de Veracruz de Ignacio de la Llave</title>
    </head>
    <body ng-controller="CtrlProveedor">
        <h1>Un Proveedor</h1>
            <div id="forma">
                <label>Escriba Id a Obtener.</label><br/>
                <label>ID:</label>
                <input type ="number" ng-model="idProveedor"><br/>
                <input type="button" value="Obtener Proveedor" ng-click="cargaUNProveedor()">
            </div>
        <br/>
        <table>
                <thead>
                    <tr>
                       <th>ID_PROVEEDOR</th>
                       <th>NOMBRE</th>
                       <th>TIPO_PROVEEDOR</th>
                       <th>DOMICILIO_FISCAL</th>
                       <th>TELEFONO_FIJO</th>
                       <th>EMAIL</th>
                       <th>BAJALOGICA</th>
                       <th>FECHA_REGISTRO</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{UnProveedor.id}}</td>
                        <td>{{UnProveedor.nombre}}</td>
                        <td>{{UnProveedor.tipoproveedor}}</td>
                        <td>{{UnProveedor.domiciliofiscal}}</td>
                        <td>{{UnProveedor.telefonofijo}}</td>
                        <td>{{UnProveedor.email}}</td>
                        <td>{{UnProveedor.bajalogica}}</td>
                        <td>{{UnProveedor.fecharegistro | date : 'dd/MM/yyyy' }}</td>
                    </tr>
                </tbody>
            </table>
        
    </body>
</html>
