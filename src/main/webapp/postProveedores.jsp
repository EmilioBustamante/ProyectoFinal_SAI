<%-- 
    Document   : postAutos
    Created on : 09-dic-2017, 16:47:31
    Author     : EmilioIsmael
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html ng-app="appProveedores">
    <head>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.6/angular.js"></script>
        <script src="js/Proveedores.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Proveedores</title>
    </head>
    <body ng-controller="CtrlProveedor">
        <h1>Proveedores de Gobierno Estatal</h1>
        <br/>
        <label>Nombre:</label>
        <input type="text" ng-model="nuevoProveedor.nombre"><br/>
        <label>Tipo Proveedor:</label>
        <input type="number" ng-model="nuevoProveedor.tipoproveedor"><br/>
        <label>Domicilio Fiscal:</label>
        <input type="text" ng-model="nuevoProveedor.domiciliofiscal"><br/>
        <label>Teléfono Fijo:</label>
        <input type="text" ng-model="nuevoProveedor.telefonofijo"><br/>
        <label>Email:</label>
        <input type="text" ng-model="nuevoProveedor.email"><br/>
        <label>BajaLogica:</label>
        <input type="number" ng-model="nuevoProveedor.bajalogica"><br/>
        <label>Fecha_Renta:</label>
        <input type="date" ng-model="nuevoProveedor.fechaRenta"><br/>
        <br/>
        <input type="button" value="Guardar" ng-click="guardarProveedor()">
    </body>
</html>
