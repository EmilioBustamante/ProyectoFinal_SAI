/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mrysi.appsweb.SAI.control;

/**
 *
 * @author EmilioIsmael
 */

import java.util.List;
import mrysi.appsweb.SAI.entidades.Proveedores;
import mrysi.appsweb.SAI.oad.OadProveedores;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;


@RestController
@RequestMapping("/SAI2018/Proveedores")
public class ControladorProveedores {
    
    @Autowired
    OadProveedores oadProveedores;
     
    @GetMapping("")
    public List<Proveedores> getTodos() {
        try{
                oadProveedores.findAll();
                
        }catch(NullPointerException e)
        {
            System.out.println("NullPointerException caught");
        }    
        return oadProveedores.findAll();
    }
    
    
    @GetMapping("/{id}")
    public Proveedores getPorId(@PathVariable("id") Integer id) {
        try{
             oadProveedores.findOne(id);
             
        }catch(NullPointerException e)
        {
            System.out.println("NullPointerException caught");
        }
        
        return oadProveedores.findOne(id);
    }
    
    @PostMapping("")
    public Proveedores crearProveedor(@RequestBody Proveedores proveedor) {
         try{
                oadProveedores.save(proveedor);
                
        }catch(NullPointerException e)
        {
            System.out.println("NullPointerException caught");
        }
        return proveedor;
    }
    
    @PutMapping("/{id}")
    public Proveedores actualizarProveedor(@PathVariable("id") Integer id, @RequestBody Proveedores proveedor) {
         try{
                proveedor.setId(id);
                oadProveedores.save(proveedor);
                
        }catch(NullPointerException e)
        {
            System.out.println("NullPointerException caught");
        }
         return proveedor;
    }
    
    
    @DeleteMapping("/{id}")
    public void borrarProveedor(@PathVariable("id") Integer id) {
         try{
                oadProveedores.delete(id);
        }catch(NullPointerException e)
        {
            System.out.println("NullPointerException caught");
        }
    }
    
    
    
}
