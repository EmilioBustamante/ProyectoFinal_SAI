/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mrysi.appsweb.SAI.control;

/**
 *
 * @author EmilioIsmael
 */
import java.util.List;
import mrysi.appsweb.SAI.entidades.AgentesContratantes;
import mrysi.appsweb.SAI.entidades.Proveedores;
import mrysi.appsweb.SAI.oad.OadAgentesContratantes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;



@RestController
@RequestMapping("/SAI2018/Agentes")
public class ControladorAgentesContratantes {
    
    @Autowired
    OadAgentesContratantes oadAgentesContratantes;
    
    @GetMapping("")
    public List<AgentesContratantes> getTodos() {
        return oadAgentesContratantes.findAll();
    }
    
    @GetMapping("/{id}")
    public AgentesContratantes getPorId(@PathVariable("id") Integer id) {
        return oadAgentesContratantes.findOne(id);
    }
    
    @PostMapping("")
    public AgentesContratantes crearAgente(@RequestBody AgentesContratantes agente) {
        oadAgentesContratantes.save(agente);
        return agente;
    }
    
    @PutMapping("/{id}")
    public AgentesContratantes actualizarAgente(@PathVariable("id") Integer id, @RequestBody AgentesContratantes agente) {
        agente.setId(id);
        oadAgentesContratantes.save(agente);
        return agente;
    }
    
    
    @DeleteMapping("/{id}")
    public void borrarAgente(@PathVariable("id") Integer id) {
        oadAgentesContratantes.delete(id);
    }
    
    
}
