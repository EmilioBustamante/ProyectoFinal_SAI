/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mrysi.appsweb.SAI.control;

/**
 *
 * @author EmilioIsmael
 */
import java.util.Date;
import java.util.List;
import mrysi.appsweb.SAI.entidades.PropuestasConcursos;
import mrysi.appsweb.SAI.entidades.Proveedores;
import mrysi.appsweb.SAI.oad.OadPropuestasConcursos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/SAI2018/Propuestas")
public class ControladorPropuestasConcursos {
 
    @Autowired
    OadPropuestasConcursos oadPropuestasConcursos;
    
    @GetMapping("")
    public List<PropuestasConcursos> getTodos() {
        return oadPropuestasConcursos.findAll();
    }
    
    @GetMapping("/{id}")
    public PropuestasConcursos getPorId(@PathVariable("id") Integer id) {
        return oadPropuestasConcursos.findOne(id);
    }
    
    @PostMapping("")
    public PropuestasConcursos crearPropuestaConcurso(@RequestBody PropuestasConcursos propuesta) {
        oadPropuestasConcursos.save(propuesta);
        return propuesta;
    }
    
    @PutMapping("/{id}")
    public PropuestasConcursos actualizarPropuesta(@PathVariable("id") Integer id, @RequestBody PropuestasConcursos propuesta) {
        propuesta.setId(id);
        oadPropuestasConcursos.save(propuesta);
        return propuesta;
    }
    
    
    @DeleteMapping("/{id}")
    public void borrarPropuesta(@PathVariable("id") Integer id) {
        oadPropuestasConcursos.delete(id);
    }
    
}
