/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mrysi.appsweb.SAI.control;

import java.util.List;
import mrysi.appsweb.SAI.entidades.Concursos;
import mrysi.appsweb.SAI.entidades.Proveedores;
import mrysi.appsweb.SAI.oad.OadConcursos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author EmilioIsmael
 */
@RestController
@RequestMapping("/SAI2018/Concursos")
public class ControladorConcursos {
    
    @Autowired
    OadConcursos oadConcursos;
    
     @GetMapping("")
    public List<Concursos> getTodos() {
        return oadConcursos.findAll();
    }
    
    @GetMapping("/{id}")
    public Concursos getPorId(@PathVariable("id") Integer id) {
        return oadConcursos.findOne(id);
    }
    
    @PostMapping("")
    public Concursos crearConcurso(@RequestBody Concursos concurso) {
        oadConcursos.save(concurso);
        return concurso;
    }
    
    @PutMapping("/{id}")
    public Concursos actualizarConcurso(@PathVariable("id") Integer id, @RequestBody Concursos concurso) {
        concurso.setId(id);
        oadConcursos.save(concurso);
        return concurso;
    }
    
    
    @DeleteMapping("/{id}")
    public void borrarConcurso(@PathVariable("id") Integer id) {
        oadConcursos.delete(id);
    }
    
    
}
