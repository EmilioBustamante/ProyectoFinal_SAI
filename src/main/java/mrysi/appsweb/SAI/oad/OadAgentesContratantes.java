/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mrysi.appsweb.SAI.oad;

/**
 *
 * @author EmilioIsmael
 */
import java.util.Date;
import java.util.List;
import mrysi.appsweb.SAI.entidades.AgentesContratantes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface OadAgentesContratantes extends JpaRepository<AgentesContratantes, Integer> {
    
    List<AgentesContratantes> findBynombreContainingIgnoreCase(String cadena);
    
    List<AgentesContratantes> findBytipoagente(short tipo);
    
    List<AgentesContratantes> findByfecharegistroBetween(Date f1, Date f2);
    
    @Query("SELECT a FROM AGENTESCONTRATANTES a WHERE a.FECHAREGISTRO = :fecha")
    List<AgentesContratantes> buscarAgentesRegistrados(@Param("fecha") Date fecha);
    
}
