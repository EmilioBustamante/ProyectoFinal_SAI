/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mrysi.appsweb.SAI.oad;

/**
 *
 * @author EmilioIsmael
 */
import java.util.Date;
import java.util.List;
import mrysi.appsweb.SAI.entidades.PropuestasConcursos;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


public interface OadPropuestasConcursos extends JpaRepository<PropuestasConcursos, Integer> {
 
    List<PropuestasConcursos> findByarchivopdfContainingIgnoreCase(String cadena);
    
    List<PropuestasConcursos> findBytipopropuesta(short tipo); 
    
    List<PropuestasConcursos> findByidconcurso(Integer id);
    
    List<PropuestasConcursos> findByfecharegistroBetween(Date f1, Date f2);
    
    @Query("SELECT p FROM PROPUESTASCONCURSOS p WHERE p.FECHAREGISTRO = :fecha")
    List<PropuestasConcursos> buscarPropuestasRegistradas(@Param("fecha") Date fecha);
}
