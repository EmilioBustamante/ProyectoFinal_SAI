/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mrysi.appsweb.SAI.oad;

/**
 *
 * @author EmilioIsmael
 */
import java.util.Date;
import java.util.List;
import mrysi.appsweb.SAI.entidades.Proveedores;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface OadProveedores extends JpaRepository<Proveedores, Integer> {
    
    List<Proveedores> findBynombreContainingIgnoreCase(String cadena);
    
    List<Proveedores> findByemailContainingIgnoreCase(String cadena);
    
    List<Proveedores> findByfecharegistroBetween(Date f1, Date f2);
    
    @Query("SELECT v FROM PROVEEDORES v WHERE v.FECHAREGISTRO = :fecha")
    List<Proveedores> buscarProveedoresRegistrados(@Param("fecha") Date fecha);
    
}
