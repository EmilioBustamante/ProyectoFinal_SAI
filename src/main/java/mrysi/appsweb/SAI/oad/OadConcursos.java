/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mrysi.appsweb.SAI.oad;

/**
 *
 * @author EmilioIsmael
 */
import java.util.Date;
import java.util.List;
import mrysi.appsweb.SAI.entidades.Concursos;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


public interface OadConcursos extends JpaRepository<Concursos, Integer> {
    
    List<Concursos> findByconcursoContainingIgnoreCase(String cadena);
    
    List<Concursos> findByfechaconcursoBetween(Date f1, Date f2);
    
    List<Concursos> findByfecharegistroBetween(Date f1, Date f2);
    
    @Query("SELECT b FROM CONCURSOS b WHERE b.FECHAREGISTRO = :fecha")
    List<Concursos> buscarConcursosRegistrados(@Param("fecha") Date fecha);
    
}
