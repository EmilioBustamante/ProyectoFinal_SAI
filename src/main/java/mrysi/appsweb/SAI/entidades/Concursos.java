/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mrysi.appsweb.SAI.entidades;

/**
 *
 * @author EmilioIsmael
 */

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "CONCURSOS")
public class Concursos implements Serializable {
    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IDCONCURSO")
    private Integer id;
    
    @Basic(optional = false)
    @Size(min = 1, max = 100)
    @Column(name = "CONCURSO")
    private String concurso;
    
    @Basic(optional = false)
    @Column(name = "TIPOCONCURSO")
    private short tipoconcurso;
    
    @Column(name = "FECHACONCURSO")
    @Temporal(TemporalType.DATE)
    private Date fechaconcurso;
    
    @Column(name = "FECHAREGISTRO")
    @Temporal(TemporalType.DATE)
    private Date fecharegistro;
    
    @Basic(optional = false)
    @Column(name = "BAJALOGICA")
    private short bajalogica;
    
    @Basic(optional = false)
    @Size(min = 1, max = 20)
    @Column(name = "FOLIO")
    private String folio;

    public Concursos(){
    }
    
    public Concursos(Integer id) {
        this.id = id;
    }
    
    public Concursos(Integer id, String concurso, short tipoconcurso, Date fechaconcurso, Date fecharegistro, short bajalogica, String folio) {
        this.id = id;
        this.concurso = concurso;
        this.tipoconcurso = tipoconcurso;
        this.fechaconcurso = fechaconcurso;
        this.fecharegistro = fecharegistro;
        this.bajalogica = bajalogica;
        this.folio = folio;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getId() {
        return id;
    }

    public String getConcurso() {
        return concurso;
    }

    public short getTipoconcurso() {
        return tipoconcurso;
    }

    public Date getFechaconcurso() {
        return fechaconcurso;
    }

    public Date getFecharegistro() {
        return fecharegistro;
    }

    public short getBajalogica() {
        return bajalogica;
    }

    public String getFolio() {
        return folio;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setConcurso(String concurso) {
        this.concurso = concurso;
    }

    public void setTipoconcurso(short tipoconcurso) {
        this.tipoconcurso = tipoconcurso;
    }

    public void setFechaconcurso(Date fechaconcurso) {
        this.fechaconcurso = fechaconcurso;
    }

    public void setFecharegistro(Date fecharegistro) {
        this.fecharegistro = fecharegistro;
    }

    public void setBajalogica(short bajalogica) {
        this.bajalogica = bajalogica;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Concursos)) {
            return false;
        }
        Concursos other = (Concursos) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "SAI2018-Concurso[ id=" + id + " ]";
    }

    
}
