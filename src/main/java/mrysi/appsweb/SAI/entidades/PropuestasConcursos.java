/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mrysi.appsweb.SAI.entidades;

/**
 *
 * @author EmilioIsmael
 */

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "PROPUESTASCONCURSOS")
public class PropuestasConcursos implements Serializable {
    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IDPROPUESTA")
    private Integer id;
    
    @Basic(optional = false)
    @Column(name = "IDPROVEEDOR")
    private Integer idproveedor;
    
    @Basic(optional = false)
    @Column(name = "TIPOPROPUESTA")
    private short tipopropuesta;
    
    @Basic(optional = false)
    @Size(min = 1, max = 200)
    @Column(name = "ARCHIVOPDF")
    private String archivopdf;
    
    @Column(name = "FECHAREGISTRO")
    @Temporal(TemporalType.DATE)
    private Date fecharegistro;
    
    @Basic(optional = false)
    @Column(name = "BAJALOGICA")
    private short bajalogica;
    
    @Basic(optional = false)
    @Column(name = "IDCONCURSO")
    private Integer idconcurso;

    public PropuestasConcursos(){
    }
    
    public PropuestasConcursos(Integer id){
        this.id = id;
    }
    
    public PropuestasConcursos(Integer id, Integer idproveedor, short tipopropuesta, String archivopdf, Date fecharegistro, short bajalogica, Integer idconcurso) {
        this.id = id;
        this.idproveedor = idproveedor;
        this.tipopropuesta = tipopropuesta;
        this.archivopdf = archivopdf;
        this.fecharegistro = fecharegistro;
        this.bajalogica = bajalogica;
        this.idconcurso = idconcurso;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getId() {
        return id;
    }

    public Integer getIdproveedor() {
        return idproveedor;
    }

    public short getTipopropuesta() {
        return tipopropuesta;
    }

    public String getArchivopdf() {
        return archivopdf;
    }

    public Date getFecharegistro() {
        return fecharegistro;
    }

    public short getBajalogica() {
        return bajalogica;
    }

    public Integer getIdconcurso() {
        return idconcurso;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setIdproveedor(Integer idproveedor) {
        this.idproveedor = idproveedor;
    }

    public void setTipopropuesta(short tipopropuesta) {
        this.tipopropuesta = tipopropuesta;
    }

    public void setArchivopdf(String archivopdf) {
        this.archivopdf = archivopdf;
    }

    public void setFecharegistro(Date fecharegistro) {
        this.fecharegistro = fecharegistro;
    }

    public void setBajalogica(short bajalogica) {
        this.bajalogica = bajalogica;
    }

    public void setIdconcurso(Integer idconcurso) {
        this.idconcurso = idconcurso;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PropuestasConcursos)) {
            return false;
        }
        PropuestasConcursos other = (PropuestasConcursos) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "SAI2018-Propuesta[ id=" + id + " ]";
    }

    
}
