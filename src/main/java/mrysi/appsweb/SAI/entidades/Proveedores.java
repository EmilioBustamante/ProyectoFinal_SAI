/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mrysi.appsweb.SAI.entidades;

/**
 *
 * @author EmilioIsmael
 */

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "PROVEEDORES")
public class Proveedores implements Serializable {
    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IDPROVEEDOR")
    private Integer id;
    
    @Basic(optional = false)
    @Size(min = 1, max = 100)
    @Column(name = "NOMBRE")
    private String nombre;
    
    @Basic(optional = false)
    @Column(name = "TIPOPROVEEDOR")
    private short tipoproveedor;
    
    @Basic(optional = false)
    @Size(min = 1, max = 200)
    @Column(name = "DOMICILIOFISCAL")
    private String domiciliofiscal;
    
    @Basic(optional = false)
    @Size(min = 1, max = 20)
    @Column(name = "TELEFONOFIJO")
    private String telefonofijo;
    
    @Basic(optional = false)
    @Size(min = 1, max = 100)
    @Column(name = "EMAIL")
    private String email;
     
    @Basic(optional = false)
    @Column(name = "BAJALOGICA")
    private short bajalogica;
    
    @Column(name = "FECHAREGISTRO")
    @Temporal(TemporalType.DATE)
    private Date fecharegistro;

    public Proveedores(){
    }
    
    public Proveedores(Integer id){
        this.id = id;
    }
    
    public Proveedores(Integer id, String nombre, short tipoproveedor, String domiciliofiscal, String telefonofijo, String email, short bajalogica, Date fecharegistro) {
        this.id = id;
        this.nombre = nombre;
        this.tipoproveedor = tipoproveedor;
        this.domiciliofiscal = domiciliofiscal;
        this.telefonofijo = telefonofijo;
        this.email = email;
        this.bajalogica = bajalogica;
        this.fecharegistro = fecharegistro;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public short getTipoproveedor() {
        return tipoproveedor;
    }

    public String getDomiciliofiscal() {
        return domiciliofiscal;
    }

    public String getTelefonofijo() {
        return telefonofijo;
    }

    public String getEmail() {
        return email;
    }

    public short getBajalogica() {
        return bajalogica;
    }

    public Date getFecharegistro() {
        return fecharegistro;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setTipoproveedor(short tipoproveedor) {
        this.tipoproveedor = tipoproveedor;
    }

    public void setDomiciliofiscal(String domiciliofiscal) {
        this.domiciliofiscal = domiciliofiscal;
    }

    public void setTelefonofijo(String telefonofijo) {
        this.telefonofijo = telefonofijo;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setBajalogica(short bajalogica) {
        this.bajalogica = bajalogica;
    }

    public void setFecharegistro(Date fecharegistro) {
        this.fecharegistro = fecharegistro;
    }
 
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Proveedores)) {
            return false;
        }
        Proveedores other = (Proveedores) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "SAI2018-Proveedor[ id=" + id + " ]";
    }

    
    
}
