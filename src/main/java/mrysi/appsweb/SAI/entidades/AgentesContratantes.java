/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mrysi.appsweb.SAI.entidades;

/**
 *
 * @author EmilioIsmael
 */

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "AGENTESCONTRATANTES")
public class AgentesContratantes implements Serializable {
    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IDAGENTE")
    private Integer id;
    
    @Basic(optional = false)
    @Size(min = 1, max = 100)
    @Column(name = "NOMBRE")
    private String nombre;
    
    @Basic(optional = false)
    @Column(name = "IDNIVEL")
    private short idnivel;
    
    @Basic(optional = false)
    @Column(name = "TIPOAGENTE")
    private short tipoagente;
    
    @Basic(optional = false)
    @Column(name = "BAJALOGICA")
    private short bajalogica;
    
    @Column(name = "FECHAREGISTRO")
    @Temporal(TemporalType.DATE)
    private Date fecharegistro;

    
    public AgentesContratantes(){
    }
    
    public AgentesContratantes(Integer id){
        this.id = id;
    }
    
    public AgentesContratantes(Integer id, String nombre, short idnivel, short tipoagente, short bajalogica, Date fecharegistro) {
        this.id = id;
        this.nombre = nombre;
        this.idnivel = idnivel;
        this.tipoagente = tipoagente;
        this.bajalogica = bajalogica;
        this.fecharegistro = fecharegistro;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public short getIdnivel() {
        return idnivel;
    }

    public short getTipoagente() {
        return tipoagente;
    }

    public short getBajalogica() {
        return bajalogica;
    }

    public Date getFecharegistro() {
        return fecharegistro;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setIdnivel(short idnivel) {
        this.idnivel = idnivel;
    }

    public void setTipoagente(short tipoagente) {
        this.tipoagente = tipoagente;
    }

    public void setBajalogica(short bajalogica) {
        this.bajalogica = bajalogica;
    }

    public void setFecharegistro(Date fecharegistro) {
        this.fecharegistro = fecharegistro;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AgentesContratantes)) {
            return false;
        }
        AgentesContratantes other = (AgentesContratantes) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "SAI2018-AgenteContratante[ id=" + id + " ]";
    }

    
    
    
    
}
